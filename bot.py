from flask import Flask
import requests
import ConfigParser
from OpenSSL import SSL

from yesnowtf.telegram import telegram
from yesnowtf.facebook import facebook

Config = ConfigParser.ConfigParser()

Config.read("dev.cfg")

token = Config.get("telegram", 'access_token')

# context = SSL.Context(SSL.SSLv23_METHOD)
# context.use_privatekey_file('priv.key')
# context.use_certificate_file('pub.pem')

app = Flask(__name__)
app.register_blueprint(telegram, url_prefix="/telegram/"+token)
app.register_blueprint(facebook, url_prefix="/facebook/")

# https handled by nginx
if __name__ == "__main___":
    app.run()

# https handled by flask
# app.run(ssl_context=("pub.pem", "priv.key"))
