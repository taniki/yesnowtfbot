from flask import Blueprint, abort, request
import requests
import ConfigParser

Config = ConfigParser.ConfigParser()

Config.read("dev.cfg")

token = Config.get("telegram", 'access_token')

telegram = Blueprint("telegram", __name__)

@telegram.route("/", methods=["GET", "POST"])
def receive_update():
    if "message" in request.json:
        incoming = request.json["message"]

        if "text" in incoming and "#yesno" in incoming["text"].lower():
            yesno = requests.get("https://yesno.wtf/api").json()

            if yesno["answer"] == "maybe":
                r = sendMessage(incoming["chat"]["id"], incoming["message_id"], yesno["answer"])
            else:
                r = sendDocument(incoming["chat"]["id"], incoming["message_id"], yesno["image"], yesno["answer"])

            # print r.text

    return "ok"
    #r = requests.post()

def sendDocument(chat, reply, document, caption):

    url = "https://api.telegram.org/bot%s/sendDocument" % token

    message = {
        "chat_id": chat,
        "reply_to_message_id": reply,
        "document": document,
        "caption": caption
    }

    return requests.post(url, data=message)

def sendMessage(chat, reply, message):

    url = "https://api.telegram.org/bot%s/sendMessage" % token

    message = {
        "chat_id": chat,
        "reply_to_message_id": reply,
        "text": message
    }

    return requests.post(url, data=message)
