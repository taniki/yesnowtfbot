from flask import Blueprint, abort, request

import requests

import ConfigParser

Config = ConfigParser.ConfigParser()
Config.read("dev.cfg")

facebook = Blueprint("facebook", __name__)

@facebook.route("/handshake", methods=["GET"])
def handshake():
    return requests.args.get("hub.challenge", "")
